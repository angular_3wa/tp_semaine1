export default interface Note {
	id:number,
	subjectId:number,
	name:string,
	chapter: string,
	content:string,
	date:Date,
}
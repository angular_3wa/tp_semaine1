import { Component, Input } from '@angular/core';

import Note from 'src/defs/note';

@Component({
  selector: 'app-notedetails',
  templateUrl: './notedetails.component.html',
  styleUrls: ['./notedetails.component.scss'],
})
export class NotedetailsComponent {
  constructor(
    ) {}
  @Input() selectNote?:Note;
}

import { Component } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { NotesService } from '../notes.service';

import Note from 'src/defs/note';

@Component({
  selector: 'app-notelist',
  templateUrl: './notelist.component.html',
  styleUrls: ['./notelist.component.scss']
})
export class NotelistComponent {
  constructor(
    private service: NotesService,
    private route:Router,
    ) {
      console.log(this.courseType);
    }

  courseType = this.route.url.split('/')[1];
  notes: Note[] = this.courseType === 'Angular' ? this.service.getAngularNotes() : this.service.getTypescriptNotes();
  selectNote?:Note;
  
  onClick(note:Note): void {
    this.selectNote = this.selectNote === note ? undefined : note;
  }
}
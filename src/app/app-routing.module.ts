import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { NotedetailsComponent } from './notedetails/notedetails.component';
import { NotelistComponent } from './notelist/notelist.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'subject/:subjectId', component: NotelistComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

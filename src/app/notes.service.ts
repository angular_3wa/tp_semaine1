import { Injectable } from '@angular/core';

import Note from 'src/defs/note';

import { COURSES as courses } from 'src/data/courses';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor() { }

  getAllNotes():Note[] {
    return courses;
  }

  getAngularNotes():Note[] {
    const angularNotes:Note[] = [];
    courses.forEach(el => {
      el.subjectId === 1 ? angularNotes.push(el) : null;
    })
    return angularNotes;
  }

  getAngularNote(id?:number):Note|undefined {
    const angularNote:Note|undefined = courses.find( i => i.id === id && i.subjectId === 1);
    return angularNote;
  }

  getTypescriptNotes():Note[] {
    const typescriptNotes:Note[] = [];
    courses.forEach(el => {
      el.subjectId === 2 ? typescriptNotes.push(el) : null;
    })
    return typescriptNotes;
  }

  getTypescriptNote(id?:number):Note|undefined {
    const typescriptNote:Note|undefined = courses.find( i => i.id === id && i.subjectId === 2);
    return typescriptNote;
  }
}

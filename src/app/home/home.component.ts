import { Component } from '@angular/core';

import { NotesService } from '../notes.service';

import Note from 'src/defs/note';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  constructor(
    private service:NotesService,
  ) { }

  randomNote:Note = this.getRandomNote();


  getRandomNote() {
    let courseType:number = Math.floor(Math.random() * this.service.getAllNotes().length);
    return this.service.getAllNotes()[courseType];
  }
}

import Note from "src/defs/note";

export const COURSES:Note[] = [
	{
		"id": 1,
		"subjectId": 1,
		"name": "TestA1",
		"chapter": "01",
		"content": "Angular c'est super !",
		"date": new Date('2023, 4, 28'),
	},
	{
		"id": 2,
		"subjectId": 1,
		"name": "TestA2",
		"chapter": "01",
		"content": "It works!",
		"date": new Date('2023, 4, 28'),
	},
	{
		"id": 1,
		"subjectId": 2,
		"name": "TestTS1",
		"chapter": "01",
		"content": "Typescript rocks!",
		"date": new Date('2023, 4, 28'),
	},
	{
		"id": 2,
		"subjectId": 2,
		"name": "TestTS2",
		"chapter": "01",
		"content": "It works!",
		"date": new Date('2023, 4, 28'),
	},
]